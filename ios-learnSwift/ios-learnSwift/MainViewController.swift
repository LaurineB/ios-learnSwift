//
//  MainViewController.swift
//  ios-learnSwift
//
//  Created by Laurine Baillet on 02/06/2017.
//  Copyright © 2017 Laurine Baillet. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var oneTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        
        // Pass the selected object to the new view controller.
    }
 

    @IBAction func nextAction(_ sender: UIButton) {
        let vc : ViewController = navigationController?.storyboard?.instantiateViewController(withIdentifier: "viewcontroller") as! ViewController
        vc.oneLabel.text = oneTextField.text
        
        navigationController?.pushViewController(vc, animated: true)
    }
}
